#ifndef GAME_ENGINE_C_GAME_ENGINE_HPP__
#define GAME_ENGINE_C_GAME_ENGINE_HPP__

#include "rendering/IRenderingEngine.hpp"

namespace Ge
{

    struct SGameEngineCreateInfos
    {
        SRenderingEngineCreateInfos *p_rendering_engine_create_infos;
    };

    class CGameEngine
    {
    public:
        ~CGameEngine();
        void Initialize(const SGameEngineCreateInfos &game_engine_create_infos);
        void Release();
        void Run();

    private:
        bool m_initialized = false;
        IRenderingEngine *mp_rendering_engine;
    };

} // !namespace Ge

#endif // !GAME_ENGINE_C_GAME_ENGINE_HPP__