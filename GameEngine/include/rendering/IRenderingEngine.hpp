#ifndef GAME_ENGINE_I_RENDERERING_ENGINE_HPP__
#define GAME_ENGINE_I_RENDERERING_ENGINE_HPP__

#include "rendering/renderer/IRenderer.hpp"

namespace Ge
{

    enum ERenderingEngineType
    {
        Rasterization,
        RayTracingCPU,
        RayTracingGPU
    };

    struct SRenderingEngineCreateInfos
    {
        ERenderingEngineType e_rendering_engine_type;
        SRendererCreateInfos *p_renderer_create_infos;
    };

    class IRenderingEngine
    {
    public:
        virtual ~IRenderingEngine() = default;
        virtual void Initialize(const SRenderingEngineCreateInfos &rendering_engine_create_infos) = 0;
        virtual void Release() = 0;
        virtual void Render(float lag) = 0;

        IRenderer *GetRenderer();

    protected:
        bool m_initialized = false;
        IRenderer *mp_renderer = nullptr;
    };

} // !namespace Ge

#endif // !GAME_ENGINE_I_RENDERERING_ENGINE_HPP__