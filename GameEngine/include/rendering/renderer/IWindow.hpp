#ifndef GAME_ENGINE_I_WINDOW_HPP__
#define GAME_ENGINE_I_WINDOW_HPP__

#include "glew/glew.h"
#include "glfw/glfw3.h"

namespace Ge
{

    struct SWindowCreateInfos
    {
        int window_width;
        int window_height;
        const char *p_window_title;
    };

    class IWindow
    {
    public:
        virtual ~IWindow() = default;
        virtual void Initialize(const SWindowCreateInfos& window_create_infos) = 0;
        virtual void Release() = 0;

        GLFWwindow *GetHandle();

    protected:
        bool m_initialized = false;
        GLFWwindow *mp_window = nullptr;
    };

} // !namespace Ge

#endif // !GAME_ENGINE_I_WINDOW_HPP__