#include <iostream>
#include <stdexcept>

#include "CGameEngine.hpp"
#include "rendering/rasterization/CRasterizationEngine.hpp"

namespace Ge
{

    CGameEngine::~CGameEngine()
    {
        Release(); // RAII
    }

    void CGameEngine::Initialize(const SGameEngineCreateInfos &game_engine_create_infos)
    {
        if (m_initialized)
            Release();

        switch (game_engine_create_infos.p_rendering_engine_create_infos->e_rendering_engine_type)
        {
        case Rasterization:
            mp_rendering_engine = new CRasterizationEngine();
            break;

        case RayTracingCPU:
            throw std::runtime_error("CPU ray tracing engine is not implemented.");
            break;

        case RayTracingGPU:
            throw std::runtime_error("GPU ray tracing engine is not implemented.");
            break;

        default:
            throw std::runtime_error(game_engine_create_infos.p_rendering_engine_create_infos->e_rendering_engine_type + " engine is not implemented.");
            break;
        }

        if (!mp_rendering_engine)
            throw std::bad_alloc();

        m_initialized = true;
        mp_rendering_engine->Initialize(*game_engine_create_infos.p_rendering_engine_create_infos);

        std::cout << "Game engine initialized." << std::endl;
    }

    void CGameEngine::Release()
    {
        if (m_initialized)
        {
            if (mp_rendering_engine)
            {
                mp_rendering_engine->Release();
                delete mp_rendering_engine;

                mp_rendering_engine = nullptr;
            }

            std::cout << "Game engine released." << std::endl;
        }
    }

    void CGameEngine::Run()
    {
        if (!m_initialized)
            throw std::runtime_error("Game engine not initialized.");

        GLFWwindow *p_window = mp_rendering_engine->GetRenderer()->GetWindow()->GetHandle();

        // Timers
        double lag = 0.0;
        double previous = glfwGetTime();
        double SECONDS_PER_UPDATE = 1.0 / 60.0;

        while (!glfwWindowShouldClose(p_window) && glfwGetKey(p_window, GLFW_KEY_ESCAPE) != GLFW_PRESS)
        {
            double current = glfwGetTime();
            double elapsed = current - previous;
            previous = current;

            lag += elapsed;
            glfwPollEvents();

            while (lag >= SECONDS_PER_UPDATE)
            {
                // Game logic
                lag -= SECONDS_PER_UPDATE;
            }

            // Rendering
            mp_rendering_engine->Render(static_cast<float>((float)lag / SECONDS_PER_UPDATE));
        }
    }

} // !namespace Ge
