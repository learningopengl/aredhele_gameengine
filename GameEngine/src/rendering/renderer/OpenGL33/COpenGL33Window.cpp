#include <iostream>
#include <stdexcept>

#include "rendering/renderer/OpenGL33/COpenGL33Window.hpp"

namespace Ge
{
    void ErrorCallback(int error, const char *description)
    {
        throw std::runtime_error("Error : " + std::string(description));
    }

    COpenGL33Window::~COpenGL33Window()
    {
        Release(); // RAII
    }

    void COpenGL33Window::Initialize(const SWindowCreateInfos& window_create_infos)
    {
        if (m_initialized)
            Release();
        
        std::cout << "Initialize the OpenGL 3.3 window ..." << std::endl;

        if (!glfwInit())
            throw std::runtime_error("Failed to initilize GLFW.");

        glfwSetErrorCallback(ErrorCallback);
        glfwWindowHint(GLFW_SAMPLES, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        mp_window = glfwCreateWindow(window_create_infos.window_width, window_create_infos.window_height, window_create_infos.p_window_title, nullptr, nullptr);

        if (!mp_window)
        {
            glfwTerminate();
            throw std::runtime_error("Failed to initilize the window.");
        }

        glfwMakeContextCurrent(mp_window);

        glewExperimental = GL_TRUE;
        if (glewInit() != GLEW_OK)
        {
            glfwTerminate();
            throw std::runtime_error("Failed to initilize GLEW.");
        }

        glfwSetInputMode(mp_window, GLFW_STICKY_KEYS, GL_TRUE);

        m_initialized = true;

        std::cout << "OpenGL 3.3 window initialized." << std::endl;
    }

    void COpenGL33Window::Release()
    {
        if (m_initialized)
        {
            glfwTerminate();
            mp_window = nullptr;
            m_initialized = false;
            
            std::cout << "OpenGL 3.3 window released." << std::endl;
        }
    }
} // !namespace Ge
