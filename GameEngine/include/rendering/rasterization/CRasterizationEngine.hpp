#ifndef GAME_ENGINE_C_RASTERIZATION_ENGINE_HPP__
#define GAME_ENGINE_C_RASTERIZATION_ENGINE_HPP__

#include "rendering/IRenderingEngine.hpp"

namespace Ge
{

    class CRasterizationEngine : public IRenderingEngine
    {
    public:
        ~CRasterizationEngine() override;
        void Initialize(const SRenderingEngineCreateInfos &rendering_engine_create_infos) final;
        void Release() final;
        void Render(float lag) final;
    };

} // !namespace Ge

#endif // !GAME_ENGINE_C_RASTERIZATION_ENGINE_HPP__