#ifndef GAME_ENGINE_C_OPENGL_33_WINDOW_HPP__
#define GAME_ENGINE_C_OPENGL_33_WINDOW_HPP__

#include "rendering/renderer/IWindow.hpp"

namespace Ge
{

    class COpenGL33Window : public IWindow
    {
    public:
        ~COpenGL33Window() override;

        void Initialize(const SWindowCreateInfos& window_create_infos) final;
        void Release() final;
    };

} // !namespace Ge

#endif // !GAME_ENGINE_C_OPENGL_33_WINDOW_HPP__