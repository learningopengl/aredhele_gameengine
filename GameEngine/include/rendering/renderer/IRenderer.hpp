#ifndef GAME_ENGINE_I_RENDERER_HPP__
#define GAME_ENGINE_I_RENDERER_HPP__

#include "rendering/renderer/IWindow.hpp"

namespace Ge
{

    enum ERendererType
    {
        OpenGL33,
        OpenGL45,
        Vulkan
    };

    struct SRendererCreateInfos
    {
        ERendererType e_renderer_type;
        SWindowCreateInfos* p_window_create_infos;
    };

    class IRenderer
    {
    public:
        virtual ~IRenderer() = default;
        virtual void Initialize(const SRendererCreateInfos& renderer_create_infos) = 0;
        virtual void Release() = 0;

        IWindow* GetWindow();

    protected:
        bool m_initialized = false;
        IWindow* mp_window = nullptr;
    };

} // !namespace Ge

#endif // !GAME_ENGINE_I_RENDERER_HPP__