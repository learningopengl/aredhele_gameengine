#include <iostream>
#include <stdexcept>

#include "CGameEngine.hpp"

int main(int argc, char **argv)
{
    std::cout << "Game engine starting ..." << std::endl;

    Ge::SWindowCreateInfos window_create_infos{};
    window_create_infos.window_width = 1280;
    window_create_infos.window_height = 720;
    window_create_infos.p_window_title = "Game engine";

    Ge::SRendererCreateInfos renderer_create_infos{};
    renderer_create_infos.e_renderer_type = Ge::ERendererType::OpenGL33;
    renderer_create_infos.p_window_create_infos = &window_create_infos;

    Ge::SRenderingEngineCreateInfos rendering_engine_create_infos{};
    rendering_engine_create_infos.e_rendering_engine_type = Ge::ERenderingEngineType::Rasterization;
    rendering_engine_create_infos.p_renderer_create_infos = &renderer_create_infos;

    Ge::SGameEngineCreateInfos game_engine_create_infos{};
    game_engine_create_infos.p_rendering_engine_create_infos = &rendering_engine_create_infos;

    Ge::CGameEngine game_engine;

    try
    {
        game_engine.Initialize(game_engine_create_infos);
        game_engine.Run();
    }
    catch (std::runtime_error &error)
    {
        std::cerr << "Exception : " << error.what() << '\n';
    }
    catch (std::bad_alloc &error)
    {
        std::cerr << "Exception : " << error.what() << '\n';
    }

    std::cout << "Game engine stop." << std::endl;
    return 0;
}