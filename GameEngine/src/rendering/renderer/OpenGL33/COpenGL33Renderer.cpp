#include <iostream>

#include "rendering/renderer/OpenGL33/COpenGL33Renderer.hpp"

namespace Ge
{
    
    COpenGL33Renderer::~COpenGL33Renderer()
    {
        Release(); // RAII
    }

    void COpenGL33Renderer::Initialize(const SRendererCreateInfos& renderer_create_infos)
    {
        if (m_initialized)
            Release();
        
        m_window.Initialize(*renderer_create_infos.p_window_create_infos);
        mp_window = &m_window;
        
        m_initialized = true;

        std::cout << "OpenGL 3.3 renderer initialized." << std::endl;
    }

    void COpenGL33Renderer::Release()
    {
        if(m_initialized)
        {
            m_window.Release();
            mp_window = nullptr;            
            m_initialized = false;

            std::cout << "OpenGL 3.3 renderer released." << std::endl;
        }
    }

} // !namespace Ge
