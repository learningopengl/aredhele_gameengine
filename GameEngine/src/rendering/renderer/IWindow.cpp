#include "rendering/renderer/IWindow.hpp"

namespace Ge
{

    GLFWwindow *IWindow::GetHandle()
    {
        return mp_window;
    }

} // !namespace