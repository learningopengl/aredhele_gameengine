#ifndef GAME_ENGINE_C_OPENGL_33_RENDERER_HPP__
#define GAME_ENGINE_C_OPENGL_33_RENDERER_HPP__

#include "rendering/renderer/IRenderer.hpp"
#include "rendering/renderer/OpenGL33/COpenGL33Window.hpp"

namespace Ge
{
    
    class COpenGL33Renderer : public IRenderer
    {
    public:
        ~COpenGL33Renderer() override;
        void Initialize(const SRendererCreateInfos& renderer_create_infos) final;
        void Release() final;

    private:
        COpenGL33Window m_window;
    };

} // !namespace Ge

#endif // !GAME_ENGINE_C_OPENGL_33_RENDERER_HPP__