#include <iostream>
#include <stdexcept>

#include "rendering/rasterization/CRasterizationEngine.hpp"
#include "rendering/renderer/OpenGL33/COpenGL33Renderer.hpp"

namespace Ge
{

    CRasterizationEngine::~CRasterizationEngine()
    {
        Release(); // RAII
    }

    void CRasterizationEngine::Initialize(const SRenderingEngineCreateInfos& rendering_engine_create_infos)
    {
        if (m_initialized)
            Release();
        
        switch (rendering_engine_create_infos.p_renderer_create_infos->e_renderer_type)
        {
        case OpenGL33:
            mp_renderer = new COpenGL33Renderer();
            break;
        
        case OpenGL45:
            throw std::runtime_error("OpenGL 4.5 renderer not implemented.");
            break;
        
        case Vulkan:
            throw std::runtime_error("Vulkan renderer not implemented.");
            break;
        
        default:
            throw std::runtime_error(rendering_engine_create_infos.p_renderer_create_infos->e_renderer_type+" renderer not implemented.");
            break;
        }

        if(!mp_renderer)
            throw std::bad_alloc();
        
        m_initialized = true;
        mp_renderer->Initialize(*rendering_engine_create_infos.p_renderer_create_infos);

        std::cout << "Rasterization engine initialized." << std::endl;
    }

    void CRasterizationEngine::Release()
    {
        if(m_initialized)
        {
            if(mp_renderer)
            {
                mp_renderer->Release();
                delete mp_renderer;

                mp_renderer = nullptr;
            }

            m_initialized = false;
            std::cout << "Rasterization engine released." << std::endl;
        }
    }

    void CRasterizationEngine::Render(float lag)
    {
        glfwSwapBuffers(mp_renderer->GetWindow()->GetHandle());
    }

} // !namespace Ge
